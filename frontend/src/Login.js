import React from "react";
import './Login.css';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import App from './App.js'
import Registration from  './Registration';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Login() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/login" children={<Llogin />}/>
          <Route path="/registration" children={<Registration />}/>
          <Route path="/" children={<App />}/>
        </Switch>
      </div>
    </Router>
  );
}

function Llogin() {
  const classes = useStyles();

  return (
    <div id="CL">
      <h1 id="h1">Familiada Welcome!</h1>
      <div id="mid">
        <div id="information">
          <span id="textInformation">
            Familiada website invites you to play solo and multiplayer with the community of our site! Compete with the
            community of players in a game testing your knowledge of various subjects and create questions and answers
            yourself! Fight for your position in the ranking.
          </span>
        </div>
        <div id="eastBox">
          <div id="center">
            <form className={classes.root} noValidate autoComplete="off">
              <span class="text">LOGIN</span><br/>
              <TextField id="outlined-basic" label="Login" variant="outlined" /><br/>
              <span class="text">PASSWORD</span><br/>
              <TextField id="outlined-basic" label="Password" variant="outlined" /><br/>
              <Link id="decorationRegister" to="/registration">Register</Link><br/><br/>
              <Link id="decorationRegister2">Have you forgotten your password?</Link>
              <Link id="decorationNone" to="/"><br/><br/>
                <Button id="mod" variant="contained" color="secondary">
                  SIGN IN
                </Button>
              </Link>
            </form>
          </div>
        </div>
      </div>
      <div id="f">Robert Klinger &copy; 2021</div>
    </div>
  );
}