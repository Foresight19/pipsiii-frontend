import React from "react";
import './Registration.css';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Registration() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/registration" children={<RegistrationF />}/>
        </Switch>
      </div>
    </Router>
  );
}

function RegistrationF() {
  const classes = useStyles();

  return (
    <div id="CR">
      <h1 id="h1R">Familiada Welcome!</h1>
      <form className={classes.root} noValidate autoComplete="off">
        <div id="midR">
            <div id="boxAvatarR">
                <Avatar id="avatarR"/>
            </div>
            <div id="formBoxR1">
                <span>Nick</span>
                <TextField id="filled-basic" label="Nick" variant="filled" />
                <p class="marginB0R">Email</p>
                <TextField id="filled-basic" label="Email" variant="filled" />
                <div id="checkboxR">
                  <input type="checkbox" />
                  <span> Akceptuje warunki umowy</span>
                </div>
            </div>
            <div id="formBoxR2">
                <span>Login</span>
                <TextField id="filled-basic" label="Login" variant="filled" />
                <p class="marginB0R">Password</p>
                <TextField id="filled-basic" label="Password" variant="filled" />
                <Button id="buttonR" variant="contained" color="secondary">
                    REGISTER
                </Button>
            </div>
        </div>
      </form>
      <div id="fR">Robert Klinger &copy; 2021</div>
    </div>
  );
}